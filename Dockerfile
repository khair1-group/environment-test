# This Dockerfile uses an Ubuntu 20.04 LTS container with Tanzu tools & adds kubectl(latest), helm(latest 3.x), terraform (latest) & GitLab's Terraform extensions (latest) to it.

FROM node:latest

LABEL maintainer="khair@gitlab.com"
LABEL version="20200810"
LABEL description="Eleventy"

WORKDIR /tmp

RUN apt-get update && apt-get upgrade -yq && \
    npm install -g @11ty/eleventy

